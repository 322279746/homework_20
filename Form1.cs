﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace q1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Enabled = false;
            Image myimage = new Bitmap(@"hhh.jpg");
            this.BackgroundImage = myimage;

            Point ExitLocation = new Point(620, 60);
            System.Windows.Forms.Button ExitButton = new Button();
            ExitButton.Name = "Exit";
            ExitButton.Text = "Exit";
            ExitButton.Location = ExitLocation;
            this.Controls.Add(ExitButton);

            Thread ListenServ = new Thread(ListenToServer);
            ListenServ.Start();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        public static class Globals
        {
            public static String input = "";
            public static String CurrentInput = "";
            public static int EnemySource = 0;
            public static int YourSourced = 0;
            public static int was = 1;
        }

        private void GenerateCards(NetworkStream clientStream)
        {
            int x = 50, y = 550;
            Point nextLocation = new Point(x,y);
            Point enemyLocation = new Point(600,100);

            string[] imageNames = { "_10_of_clubs", "_10_of_hearts", "_10_of_spades", "_2_of_diamonds", "_2_of_hearts", "_2_of_spades",
                                  "_3_of_clubs", "_3_of_diamonds", "_3_of_hearts", "_3_of_spades", "_4_of_clubs", "_4_of_diamonds",
                                  "_4_of_hearts", "_4_of_spades", "_5_of_clubs", "_5_of_diamonds", "_5_of_hearts", "_5_of_spades",
                                  "_6_of_clubs", "_6_of_diamonds", "_6_of_hearts", "_6_of_spades", "_7_of_clubs", "_7_of_diamonds",
                                  "_7_of_hearts", "_7_of_spades", "_8_of_clubs", "_8_of_diamonds", "_8_of_hearts", "_8_of_spades",
                                  "_9_of_clubs", "_9_of_diamonds", "_9_of_hearts", "_9_of_spades", "ace_of_clubs", "ace_of_diamonds",
                                  "ace_of_hearts", "ace_of_spades2", "jack_of_clubs2", "jack_of_diamonds2", "jack_of_hearts2",
                                  "jack_of_spades2", "king_of_clubs2", "king_of_diamonds2", "king_of_hearts2", "king_of_spades2",
                                  "queen_of_clubs2", "queen_of_diamonds2", "queen_of_hearts2", "queen_of_spades2",  "_10_of_diamonds", 
                                  "_2_of_clubs"};

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("_10_of_clubs", "110C"); dictionary.Add("_10_of_hearts", "110H"); dictionary.Add("_10_of_spades", "110S");
            dictionary.Add("_2_of_diamonds", "102D"); dictionary.Add("_2_of_hearts", "102H"); dictionary.Add("_2_of_spades", "102S");
            dictionary.Add("_3_of_clubs", "103C"); dictionary.Add("_3_of_diamonds", "103D"); dictionary.Add("_3_of_hearts", "103H");
            dictionary.Add("_3_of_spades", "103S"); dictionary.Add("_4_of_clubs", "104C"); dictionary.Add("_4_of_diamonds", "104D");
            dictionary.Add("_4_of_hearts", "104H"); dictionary.Add("_4_of_spades", "104S"); dictionary.Add("_5_of_clubs", "105C");
            dictionary.Add("_5_of_diamonds", "105D"); dictionary.Add("_5_of_hearts", "105H"); dictionary.Add("_5_of_spades", "105S");
            dictionary.Add("_6_of_clubs", "106C"); dictionary.Add("_6_of_diamonds", "106D"); dictionary.Add("_6_of_hearts", "106H");
            dictionary.Add("_6_of_spades", "106S"); dictionary.Add("_7_of_clubs", "107C"); dictionary.Add("_7_of_diamonds", "107D");
            dictionary.Add("_7_of_hearts", "107H"); dictionary.Add("_7_of_spades", "107S"); dictionary.Add("_8_of_clubs", "108C");
            dictionary.Add("_8_of_diamonds", "108D"); dictionary.Add("_8_of_hearts", "108H"); dictionary.Add("_8_of_spades", "108S");
            dictionary.Add("_9_of_clubs", "109C"); dictionary.Add("_9_of_diamonds", "109D"); dictionary.Add("_9_of_hearts", "109H");
            dictionary.Add("_9_of_spades", "109S"); dictionary.Add("ace_of_clubs", "101C"); dictionary.Add("ace_of_diamonds", "101D");
            dictionary.Add("ace_of_hearts", "101H"); dictionary.Add("ace_of_spades2", "101S"); dictionary.Add("jack_of_clubs2", "111C");
            dictionary.Add("jack_of_diamonds2", "111D"); dictionary.Add("jack_of_hearts2", "111H"); dictionary.Add("jack_of_spades2", "111S");
            dictionary.Add("king_of_clubs2", "113C"); dictionary.Add("king_of_diamonds2", "113D"); dictionary.Add("king_of_hearts2", "113H");
            dictionary.Add("king_of_spades2", "113S"); dictionary.Add("queen_of_clubs2", "112C"); dictionary.Add("queen_of_diamonds2", "112D");
            dictionary.Add("queen_of_hearts2", "112H"); dictionary.Add("queen_of_spades2", "112S"); dictionary.Add("_10_of_diamonds", "110D");
            dictionary.Add("_2_of_clubs", "102C");

            Point yourSourceLocation = new Point(50, 60);
            System.Windows.Forms.Label yourSource = new Label();
            yourSource.Name = "YourSource";
            yourSource.Text = "Your Source: 0";
            yourSource.Location = yourSourceLocation;
            this.Controls.Add(yourSource);

            Point enemySourceLocation = new Point(1200, 60);
            System.Windows.Forms.Label enemySource = new Label();
            enemySource.Name = "EnemySource";
            enemySource.Text = "Enemy Source: 0";
            enemySource.Location = enemySourceLocation;
            this.Controls.Add(enemySource);

            System.Windows.Forms.PictureBox enemyPic = new PictureBox();
            enemyPic.Name = "enemy";
            enemyPic.Image = global::q1.Properties.Resources.card_back_blue;
            enemyPic.Location = enemyLocation;
            enemyPic.Size = new System.Drawing.Size(110, 110);
            enemyPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Controls.Add(enemyPic);

            object previous = null;
            int ined = 1;

            for (int i = 0; i < 10; i++)
            {
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i;
                currentPic.Image = global::q1.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(110, 110);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;         


                currentPic.Click += delegate(object sender1, EventArgs e1)
                                        {
                                            if(ined != 1)
                                            {
                                                ((PictureBox)previous).Image = global::q1.Properties.Resources.card_back_red;
                                                ((PictureBox)previous).Size = new System.Drawing.Size(110, 110);
                                                ((PictureBox)previous).SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                                                this.Controls.Add(((PictureBox)previous));

                                                enemyPic.Image = global::q1.Properties.Resources.card_back_blue;
                                                enemyPic.Location = enemyLocation;
                                                enemyPic.Size = new System.Drawing.Size(110, 110);
                                                enemyPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                                                this.Controls.Add(enemyPic);
                                            }

                                            string currentIndex = currentPic.Name.Substring(currentPic.Name.Length - 1);
                                            Point l = new Point(20, 50);
                                            var rand = new Random();
                                            string imageName = imageNames[rand.Next(imageNames.Length)];                                        
                                            object O = global::q1.Properties.Resources.ResourceManager.GetObject(imageName);
                                            ((PictureBox)sender1).Image = (Image)O;
                                            ((PictureBox)sender1).Size = new System.Drawing.Size(110, 110);
                                            ((PictureBox)sender1).SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                                            this.Controls.Add(((PictureBox)sender1));

                                           

                                            byte[] buffer = new ASCIIEncoding().GetBytes(dictionary[imageName]);
                                            clientStream.Write(buffer, 0, 4);
                                            clientStream.Flush();
                                            Globals.CurrentInput = (dictionary[imageName]);

                                            Thread equalThread = new Thread(() => equal(dictionary, currentPic, enemyPic, enemySource, yourSource));
                                            equalThread.Start();

                                            previous = sender1;
                                            ined++;
                                        };

                this.Controls.Add(currentPic);

                nextLocation.X += x + 80;
            }
        }

        private void ListenToServer(object obj)
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820); 
            client.Connect(serverEndPoint); 
            NetworkStream clientStream = client.GetStream();
            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            string ReadFromClient = new ASCIIEncoding().GetString(bufferIn);
            

            this.BeginInvoke((Action)delegate()
            {
                this.Enabled = true;
                this.GenerateCards(clientStream);
            });

            Thread ReadServ = new Thread(() => ReadFromServ(clientStream));
            ReadServ.Start();
        }

        private void ReadFromServ(NetworkStream clientStream)
        {
            while ((!((Globals.input).Equals("2000"))))
            {
                byte[] buffer = new byte[4];
                int bytesRead = clientStream.Read(buffer, 0, 4);
                Globals.input = new ASCIIEncoding().GetString(buffer);

            }
        }

        private void equal(Dictionary<string, string> dictionary, System.Windows.Forms.PictureBox currentPic, System.Windows.Forms.PictureBox enemyPic, System.Windows.Forms.Label enemySource, System.Windows.Forms.Label YourSource)
        {
            while(((Globals.input).Equals("")) || ((Globals.CurrentInput).Equals("")))
            { }

            this.BeginInvoke((Action)delegate()
            {
                var result = (from val in dictionary.Keys where (Globals.input) == dictionary[val] select val).First();
                object EnemyO = global::q1.Properties.Resources.ResourceManager.GetObject(result);
                enemyPic.Image = (Image)EnemyO;
                enemyPic.Size = new System.Drawing.Size(110, 110);
                enemyPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                this.Controls.Add(enemyPic);
                string enemyValue = (Globals.input).Substring(1, 2);
                string yourValue = (Globals.CurrentInput).Substring(1, 2);
                int IEnemyValue = int.Parse(enemyValue);
                int IYourValue = int.Parse(yourValue);

                if (IEnemyValue > IYourValue)
                {
                    (Globals.EnemySource)++;
                    enemySource.Text = "Enemy Source: " + (Globals.EnemySource);
                    this.Controls.Add(enemySource);
                }

                else if (IEnemyValue < IYourValue)
                {
                    (Globals.YourSourced)++;
                    YourSource.Text = "Enemy Source: " + (Globals.YourSourced);
                    this.Controls.Add(YourSource);
                }

                Globals.CurrentInput = "";
                Globals.input = "";
                    
            });
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
